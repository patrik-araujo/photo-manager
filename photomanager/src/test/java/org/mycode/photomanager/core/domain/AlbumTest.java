/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.domain;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class AlbumTest {

	@Test
	public void testAlbumInstance() {
		String albumTitle = "my album";
		Long albumId = 31L;

		String albumModifiedTitle = "my album title";
		Long albumModifiedId = 17L;

		Album album = new Album(albumTitle);
		assertThat(album.getTitle(), notNullValue());
		assertThat(album.getTitle(), is(albumTitle));
		assertThat(album.getId(), nullValue());
		assertThat(album.getPhotos(), notNullValue());
		assertThat(album.getPhotos().isEmpty(), notNullValue());

		album = new Album(albumId, albumTitle);
		assertThat(album.getTitle(), notNullValue());
		assertThat(album.getTitle(), is(albumTitle));
		assertThat(album.getId(), notNullValue());
		assertThat(album.getId(), is(albumId));
		assertThat(album.getPhotos(), notNullValue());
		assertThat(album.getPhotos().isEmpty(), notNullValue());

		album.setTitle(albumModifiedTitle);
		assertThat(album.getTitle(), is(albumModifiedTitle));
		album.setId(albumModifiedId);
		assertThat(album.getId(), is(albumModifiedId));
	}

	@Test
	public void testAddPhoto() {
		String albumTitle = "my album";
		Long albumId = 31L;
		String photoUrl = "photo URL";
		String photoTitle = "photo title";
		Long photoId = 17L;
		Album album = new Album(albumId, albumTitle);
		album.addPhoto(new Photo(photoId, photoUrl, photoTitle, null));

		assertThat(album.getPhotos().isEmpty(), is(false));
		assertThat(album.getPhotos().size(), is(1));
		Photo photo = album.getPhotos().get(0);
		assertThat(photo.getId(), is(photoId));
		assertThat(photo.getUrl(), is(photoUrl));
		assertThat(photo.getAlbumId(), is(albumId));
		assertThat(photo.getAlbum(), notNullValue());
		assertThat(photo.getAlbum(), is(album));
	}

}
