/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.service;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mycode.photomanager.core.service.PhotoService.ALBUM_NOT_FOUND_FOR_ID;
import static org.mycode.photomanager.core.service.PhotoService.ALBUM_REQUIRED_FOR_PHOTO;
import static org.mycode.photomanager.core.service.PhotoService.PHOTO_CANNOT_HAVE_ID_CREATION;
import static org.mycode.photomanager.core.service.PhotoService.PHOTO_ID_REQUIRED;
import static org.mycode.photomanager.core.service.PhotoService.PHOTO_NOT_FOUND_FOR_ID;
import static org.mycode.photomanager.core.service.PhotoService.PHOTO_TITLE_AT_LEAST_3_CHARS;
import static org.mycode.photomanager.core.service.PhotoService.TITLE_REQUIRED_FOR_PHOTO;
import static org.mycode.photomanager.core.service.PhotoService.URL_REQUIRED_FOR_PHOTO;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.persistence.AlbumSpringRepository;
import org.mycode.photomanager.core.persistence.PhotoSpringRepository;

public class PhotoServiceTest {

	private PhotoService photoService;
	private AlbumSpringRepository albumRepository;
	private PhotoSpringRepository photoRepository;
	

	@Before
	public void setup() {
		AlbumSpringRepository mockAlbumRepository = mock(AlbumSpringRepository.class);
		when(mockAlbumRepository.findOne(1L)).thenReturn(new Album(1L, "Mocked Album"));
		when(mockAlbumRepository.findOne(2L)).thenReturn(null);

		PhotoSpringRepository mockPhotoRepository = Mockito.mock(PhotoSpringRepository.class);
		when(mockPhotoRepository.save(Mockito.any(Photo.class))).thenReturn(new Photo(1L, "photoUrl", "photoTitle", 3L));
		
		when(mockPhotoRepository.findOne(1L)).thenReturn(new Photo(1L, "photoUrl", "photoTitle", 3L));
		when(mockPhotoRepository.findOne(2L)).thenReturn(null);
		doNothing().when(mockPhotoRepository).delete(Mockito.anyLong());
		when(mockPhotoRepository.findOne(4L)).thenReturn(new Photo(4L, "photoUrl", "photoTitle", 3L));

		List<Photo> photos = new ArrayList<>();
		photos.add(new Photo(17L, "photoUrl", "photoTitle", 3L));
		photos.add(new Photo(31L, "anyPhotoUrl", "anyPhotoTitle", 3L));
		when(mockPhotoRepository.findAll()).thenReturn(photos);

		this.albumRepository = mockAlbumRepository;
		this.photoRepository = mockPhotoRepository;
		photoService = new PhotoService(photoRepository, albumRepository);
	}

	@Test
	public void testCreateValidPhoto() {
		Photo photo = new Photo("Photo url", "Photo title", 1L);
		photo = photoService.createPhoto(photo);
		assertThat(photo, notNullValue());
		assertThat(photo.getId(), notNullValue());
	}

	@Test
	public void testCreateInvalidPhoto() {
		try {
			photoService.createPhoto(new Photo(17L, "Photo url", "Photo title", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_CANNOT_HAVE_ID_CREATION));
		}
		
		try {
			photoService.createPhoto(new Photo("Photo url", "Photo title", null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_REQUIRED_FOR_PHOTO));
		}

		try {
			photoService.createPhoto(new Photo("Photo url", null, 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.createPhoto(new Photo("Photo url", "", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.createPhoto(new Photo("Photo url", "as", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_TITLE_AT_LEAST_3_CHARS));
		}
		
		try {
			photoService.createPhoto(new Photo(null, "foo", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(URL_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.createPhoto(new Photo("", "foo", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(URL_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.createPhoto(new Photo("photo url", "photo title", 2L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_NOT_FOUND_FOR_ID));
		}
	}

	@Test
	public void testUpdatePhotoValid() {
		Photo album = photoService.updatePhoto(new Photo(1L, "Photo url", "Photo title", 1L));
		assertThat(album, notNullValue());
		assertThat(album.getId(), notNullValue());
	}

	@Test
	public void testUpdatePhotoInvalid() {
		try {
			photoService.updatePhoto(new Photo("Photo url", "Photo title", null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_ID_REQUIRED));
		}
		
		try {
			photoService.updatePhoto(new Photo(1L, "Photo url", "Photo title", null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_REQUIRED_FOR_PHOTO));
		}

		try {
			photoService.updatePhoto(new Photo(1L, "Photo url", null, 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.updatePhoto(new Photo(1L, "Photo url", "", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.updatePhoto(new Photo(1L, "Photo url", "as", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_TITLE_AT_LEAST_3_CHARS));
		}
		
		try {
			photoService.updatePhoto(new Photo(1L, null, "foo", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(URL_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.updatePhoto(new Photo(1L, "", "foo", 1L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(URL_REQUIRED_FOR_PHOTO));
		}
		
		try {
			photoService.updatePhoto(new Photo(1L, "photo url", "photo title", 2L));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_NOT_FOUND_FOR_ID));
		}
	}

	@Test
	public void testDeletePhotoValid() {
		try {
			photoService.deletePhoto(4L);
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test." + e );
		}
	}

	@Test
	public void testDeletePhotoInvalid() {
		try {
			photoService.deletePhoto(null);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_ID_REQUIRED));
		}

		try {
			photoService.deletePhoto(2L);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_NOT_FOUND_FOR_ID));
		}
	}


	@Test
	public void testFindById() {
		try {
			Photo photo = photoService.findById(1L);
			assertThat(photo, notNullValue());
			assertThat(photo.getId(), notNullValue());
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test." + e);
		}

		try {
			photoService.findById(2L);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_NOT_FOUND_FOR_ID));
		}

		try {
			photoService.findById(null);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(PHOTO_ID_REQUIRED));
		}
	}

	@Test
	public void testFindAll() {
		try {
			List<Photo> iterable = photoService.findAll();
			assertThat(iterable, notNullValue());
			assertThat(iterable.isEmpty(), is(false));
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test." + e);
		}

	}

}
