/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.domain;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class PhotoTest {

	@Test
	public void testPhotoInstance() {
		String photoUrl = "photo URL";
		String photoTitle = "photo title";
		Long photoId = 17L;
		Long albumId = 31L;

		String photoModifiedTitle = "my photo title";
		Long photoModifiedId = 32L;
		String photoModifiedUrl = "my photo url";
		Long photoModifiedAlbumId = 51L;

		Photo photo = new Photo(photoId, photoTitle, photoUrl);
		assertThat(photo.getTitle(), is(photoTitle));
		assertThat(photo.getId(), is(photoId));
		assertThat(photo.getUrl(), is(photoUrl));
		assertThat(photo.getAlbumId(), nullValue());
		assertThat(photo.getAlbum(), nullValue());

		photo = new Photo(photoId, photoUrl, photoTitle, albumId);
		assertThat(photo.getTitle(), is(photoTitle));
		assertThat(photo.getId(), is(photoId));
		assertThat(photo.getUrl(), is(photoUrl));
		assertThat(photo.getAlbumId(), is(albumId));
		assertThat(photo.getAlbum(), nullValue());

		photo.setTitle(photoModifiedTitle);
		assertThat(photo.getTitle(), is(photoModifiedTitle));
		photo.setId(photoModifiedId);
		assertThat(photo.getId(), is(photoModifiedId));
		photo.setUrl(photoModifiedUrl);
		assertThat(photo.getUrl(), is(photoModifiedUrl));
		photo.setAlbumId(photoModifiedAlbumId);
		assertThat(photo.getAlbumId(), is(photoModifiedAlbumId));
	}

}
