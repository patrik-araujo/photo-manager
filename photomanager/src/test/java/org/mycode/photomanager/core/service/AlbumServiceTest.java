/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.service;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mycode.photomanager.core.service.AlbumService.ALBUM_CANNOT_HAVE_ID_CREATION;
import static org.mycode.photomanager.core.service.AlbumService.ALBUM_CONTAINS_INVALID_PHOTOS;
import static org.mycode.photomanager.core.service.AlbumService.ALBUM_CONTAINS_PHOTOS;
import static org.mycode.photomanager.core.service.AlbumService.ALBUM_ID_REQUIRED;
import static org.mycode.photomanager.core.service.AlbumService.ALBUM_NOT_FOUND_FOR_ID;
import static org.mycode.photomanager.core.service.AlbumService.ALBUM_TITLE_AT_LEAST_3_CHARS;
import static org.mycode.photomanager.core.service.AlbumService.TITLE_IS_REQUIRED_FOR_ALBUM;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.InvalidEntityException;
import org.mycode.photomanager.core.persistence.AlbumSpringRepository;
import org.mycode.photomanager.core.persistence.PhotoSpringRepository;

public class AlbumServiceTest {

	private AlbumService albumService;
	private AlbumSpringRepository albumRepository;
	private PhotoSpringRepository photoRepository;
	private PhotoService photoService;

	@Before
	public void setup() {
		AlbumSpringRepository mockAlbumRepository = Mockito.mock(AlbumSpringRepository.class);
		Mockito.when(mockAlbumRepository.save(Mockito.any(Album.class))).thenReturn(new Album(1L, "Mocked Album"));
		Mockito.when(mockAlbumRepository.findOne(1L)).thenReturn(new Album(1L, "Mocked Album"));
		Mockito.when(mockAlbumRepository.findOne(2L)).thenReturn(null);
		Album mockedAlbumWithPhoto = new Album(3L, "Mocked Album");
		mockedAlbumWithPhoto.addPhoto(new Photo(17L, "", "", 3L));
		Mockito.when(mockAlbumRepository.findOne(3L)).thenReturn(mockedAlbumWithPhoto);
		Mockito.when(mockAlbumRepository.findOne(4L)).thenReturn(new Album(1L, "Mocked Album"));
		Mockito.doNothing().when(mockAlbumRepository).delete(Mockito.anyLong());
		List<Album> albums = new ArrayList<>();
		albums.add(new Album(17L, "any title"));
		albums.add(new Album(31L, "my title"));
		Mockito.when(mockAlbumRepository.findAll()).thenReturn(albums);

		PhotoSpringRepository mockPhotoRepository = Mockito.mock(PhotoSpringRepository.class);
		PhotoService mockPhotoService = Mockito.mock(PhotoService.class);

		this.albumRepository = mockAlbumRepository;
		this.photoRepository = mockPhotoRepository;
		this.photoService = mockPhotoService;

		albumService = new AlbumService(albumRepository, photoRepository, photoService);

		// TODO add assertion for exception types
	}

	@Test
	public void testCreateValidAlbum() {
		Album album = new Album("test Album");
		album = albumService.createAlbum(album);
		assertThat(album, notNullValue());
		assertThat(album.getId(), notNullValue());
	}

	@Test
	public void testCreateInvalidAlbum() {
		try {
			albumService.createAlbum(new Album(1L, ""));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_CANNOT_HAVE_ID_CREATION));
		}

		try {
			albumService.createAlbum(new Album(null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_IS_REQUIRED_FOR_ALBUM));
		}

		try {
			albumService.createAlbum(new Album("my"));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_TITLE_AT_LEAST_3_CHARS));
		}

		try {
			albumService.createAlbum(new Album("my"));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_TITLE_AT_LEAST_3_CHARS));
		}

	}

	@Test
	public void testCreateAlbumWithValidPhotos() {
		List<Photo> photos = new ArrayList<>();
		photos.add(new Photo(null, "photoUrl", "photoTitle"));
		photos.add(new Photo(null, "anyPhotoUrl", "anyPhotoTitle"));
		Album album = new Album("Test Album");
		photos.forEach(photo -> album.addPhoto(photo));
		Album createdAlbum = albumService.createAlbum(album);
		assertThat(createdAlbum, notNullValue());
		assertThat(createdAlbum.getId(), notNullValue());
	}

	@Test
	public void testCreateAlbumWithInvalidPhotos() {
		PhotoService mockPhotoService = Mockito.mock(PhotoService.class);
		Mockito.doThrow(InvalidEntityException.class).when(mockPhotoService).validatePhoto(Mockito.any(Photo.class));

		this.albumService = new AlbumService(albumRepository, photoRepository, mockPhotoService);

		try {
			List<Photo> photos = new ArrayList<>();
			photos.add(new Photo(null, "photoUrl", "photoTitle"));
			photos.add(new Photo(null, "anyPhotoUrl", ""));
			Album album = new Album("Test Album");
			photos.forEach(photo -> album.addPhoto(photo));
			albumService.createAlbum(album);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_CONTAINS_INVALID_PHOTOS));
		}
	}

	@Test
	public void testUpdateAlbumValid() {
		Album album = albumService.updateAlbum(new Album(1L, "asd"));
		assertThat(album, notNullValue());
		assertThat(album.getId(), notNullValue());
	}

	@Test
	public void testUpdateAlbumInvalid() {
		try {
			albumService.updateAlbum(new Album("asd"));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_ID_REQUIRED));
		}

		try {
			albumService.updateAlbum(new Album("as"));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_ID_REQUIRED));
		}

		try {
			albumService.updateAlbum(new Album(null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_ID_REQUIRED));
		}

		try {
			albumService.updateAlbum(new Album(17L, "my"));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_TITLE_AT_LEAST_3_CHARS));
		}

		try {
			albumService.updateAlbum(new Album(17L, ""));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_IS_REQUIRED_FOR_ALBUM));
		}

		try {
			albumService.updateAlbum(new Album(17L, null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(TITLE_IS_REQUIRED_FOR_ALBUM));
		}
	}

	@Test
	public void testDeleteAlbumValid() {
		try {
			albumService.deleteAlbum(4L);
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test.");
		}

	}

	@Test
	public void testDeleteAlbumInvalid() {
		try {
			albumService.deleteAlbum(null);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_ID_REQUIRED));
		}

		try {
			albumService.deleteAlbum(2L);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_NOT_FOUND_FOR_ID));
		}

		try {
			albumService.deleteAlbum(3L);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_CONTAINS_PHOTOS));
		}
	}

	@Test
	public void testAddPhotoValid() {
		try {
			albumService.addPhoto(1L, new Photo(17L, "photoUrl", "photoTitle", null));
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test." + e);
		}
	}
	
	@Test
	public void testAddPhotoInvalid() {
		try {
			albumService.addPhoto(2L, new Photo(17L, "photoUrl", "photoTitle", null));
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_NOT_FOUND_FOR_ID));
		}
	}

	@Test
	public void testFindById() {
		try {
			Album album = albumService.findById(1L);
			assertThat(album, notNullValue());
			assertThat(album.getId(), notNullValue());
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test.");
		}

		try {
			albumService.findById(2L);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_NOT_FOUND_FOR_ID));
		}

		try {
			albumService.findById(null);
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat(e, notNullValue());
			assertThat(e.getMessage(), is(ALBUM_ID_REQUIRED));
		}
	}

	@Test
	public void testFindAll() {
		try {
			List<Album> iterable = albumService.findAll();
			assertThat(iterable, notNullValue());
			assertThat(iterable.isEmpty(), is(false));
		} catch (Exception e) {
			Assert.fail("Exception not expected. Failed test.");
		}
	}

}
