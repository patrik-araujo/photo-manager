/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.InvalidEntityFetchException;
import org.mycode.photomanager.core.exception.PhotoManagerClientException;
import org.mycode.photomanager.core.service.PhotoService;
import org.mycode.photomanager.webapp.apidomain.ApiPhoto;
import org.mycode.photomanager.webapp.exception.InternalErrorException;
import org.springframework.hateoas.Link;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class PhotoResourceSimpleTest {
	
	private PhotoService photoService; 
	
	@Before
	public void setup() {
		PhotoService mockPhotoService = Mockito.mock(PhotoService.class);
		photoService = mockPhotoService;
		
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}
	
	@Test
	public void testFindAllValid() {
		List<Photo> photos = new ArrayList<Photo>();
		photos.add(new Photo(17L, "photoUrl", "photoTitle", 1L));
		photos.add(new Photo(31L, "photoUrl31", "photoTitle31", 1L));
		photos.add(new Photo(51L, "photoUrl51", "photoTitle51", 1L));
		
		
		Mockito.when(photoService.findAll()).thenReturn(photos);
		
		PhotoResource photoResource = new PhotoResource(photoService);
		Response response = photoResource.getAllPhotos();
		assertThat(response.getStatus(), is(200));
		List<ApiPhoto> responseAlbums = (List<ApiPhoto>)response.getEntity();
		assertThat(responseAlbums.isEmpty(), is(false));
		responseAlbums.forEach(apiPhoto -> assertPhotoResourceLinks(apiPhoto, false));
	}
	
	@Test
	public void testFindAllClientException() {
		Mockito.when(photoService.findAll()).thenThrow(PhotoManagerClientException.class);
		PhotoResource photoResource = new PhotoResource(photoService);
		try {
			photoResource.getAllPhotos();
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof PhotoManagerClientException), is(true));
		}
	}
	
	@Test
	public void testFindAllInternalException() {
		Mockito.when(photoService.findAll()).thenThrow(NullPointerException.class);
		PhotoResource photoResource = new PhotoResource(photoService);
		try {
			photoResource.getAllPhotos();
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof InternalErrorException), is(true));
		}
	}
	
	@Test
	public void testFindByIdValid() {
		Album album31 = new Album(1L, "album 31");
		Photo photo = new Photo(17L, "photoUrl", "photoTitle", 1L);
		album31.addPhoto(photo);
		
		Mockito.when(photoService.findById(17L)).thenReturn(photo);
		
		
		PhotoResource photoResource = new PhotoResource(photoService);
		Response response = photoResource.getPhotoById("17");
		assertThat(response.getStatus(), is(200));
		ApiPhoto responsePhoto = (ApiPhoto)response.getEntity();
		assertThat(responsePhoto, notNullValue());
		assertThat(responsePhoto.getAlbumId(), notNullValue());
		assertPhotoResourceLinks(responsePhoto, false);
	}
	
	@Test
	public void testFindByIdClientException() {
		Mockito.when(photoService.findById(17L)).thenThrow(PhotoManagerClientException.class);
		PhotoResource photoResource = new PhotoResource(photoService);
		try {
			photoResource.getPhotoById("17");
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof PhotoManagerClientException), is(true));
		}
	}
	
	@Test
	public void testFindByIdInternalException() {
		Mockito.when(photoService.findById(17L)).thenThrow(NullPointerException.class);
		PhotoResource photoResource = new PhotoResource(photoService);
		try {
			photoResource.getPhotoById("17");
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof InternalErrorException), is(true));
		}
	}
	
	@Test
	public void testFindByIdParameterFormatException() {
		Mockito.when(photoService.findById(17L)).thenThrow(NumberFormatException.class);
		PhotoResource photoResource = new PhotoResource(photoService);
		try {
			photoResource.getPhotoById("hgf543f");
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof InvalidEntityFetchException), is(true));
		}
	}
	
	private void assertPhotoResourceLinks(ApiPhoto apiPhoto, boolean selfOnly) {
		List<Link> links = apiPhoto.getLinks();
		assertThat(links.isEmpty(), is(false));
		assertThat(links.get(0).getRel(), is("self"));
		assertThat(links.get(0).getHref(), notNullValue());
		if (!selfOnly){
			assertThat(links.get(1).getRel(), is("album"));
			assertThat(links.get(1).getHref(), notNullValue());
		}
	}

}
