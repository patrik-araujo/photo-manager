/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.InvalidEntityFetchException;
import org.mycode.photomanager.core.exception.PhotoManagerClientException;
import org.mycode.photomanager.core.service.AlbumService;
import org.mycode.photomanager.webapp.apidomain.ApiAlbum;
import org.mycode.photomanager.webapp.apidomain.ApiPhoto;
import org.mycode.photomanager.webapp.exception.InternalErrorException;
import org.springframework.hateoas.Link;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class AlbumResourceSimpleTest {
	
	private AlbumService albumService; 
	
	@Before
	public void setup() {
		AlbumService mockAlbumService = Mockito.mock(AlbumService.class);
		albumService = mockAlbumService;
		
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}
	
	@Test
	public void testFindAllValid() {
		List<Album> albums = new ArrayList<Album>();
		Album album31 = new Album(31L, "album 31");
		album31.addPhoto(new Photo(17L, "photoUrl", "photoTitle", 31L));
		album31.addPhoto(new Photo(18L, "photoUrl18", "photoTitle18", 31L));
		albums.add(album31);
		albums.add(new Album(32L, "album 32"));
		albums.add(new Album(33L, "album 33"));
		albums.add(new Album(34L, "album 34"));
		albums.add(new Album(35L, "album 35"));
		Mockito.when(albumService.findAll()).thenReturn(albums);
		
		
		AlbumResource albumResource = new AlbumResource(albumService);
		Response response = albumResource.getAllAlbums();
		assertThat(response.getStatus(), is(200));
		List<ApiAlbum> responseAlbums = (List<ApiAlbum>)response.getEntity();
		assertThat(responseAlbums.isEmpty(), is(false));
		responseAlbums.forEach(apiAlbum -> assertAlbumResourceLinks(apiAlbum, true));
	}
	
	@Test
	public void testFindAllClientException() {
		Mockito.when(albumService.findAll()).thenThrow(PhotoManagerClientException.class);
		AlbumResource albumResource = new AlbumResource(albumService);
		try {
			albumResource.getAllAlbums();
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof PhotoManagerClientException), is(true));
		}
	}
	
	@Test
	public void testFindAllInternalException() {
		Mockito.when(albumService.findAll()).thenThrow(NullPointerException.class);
		AlbumResource albumResource = new AlbumResource(albumService);
		try {
			albumResource.getAllAlbums();
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof InternalErrorException), is(true));
		}
	}
	
	@Test
	public void testFindByIdValid() {
		Album album31 = new Album(1L, "album 31");
		album31.addPhoto(new Photo(17L, "photoUrl", "photoTitle", 1L));
		album31.addPhoto(new Photo(18L, "photoUrl18", "photoTitle18", 1L));
		Mockito.when(albumService.findById(1L)).thenReturn(album31);
		
		
		AlbumResource albumResource = new AlbumResource(albumService);
		Response response = albumResource.getAlbumById("1");
		assertThat(response.getStatus(), is(200));
		ApiAlbum responseAlbum = (ApiAlbum)response.getEntity();
		assertThat(responseAlbum, notNullValue());
		assertThat(responseAlbum.getPhotos().isEmpty(), is(false));
		assertAlbumResourceLinks(responseAlbum, true);
	}
	
	@Test
	public void testFindByIdClientException() {
		Mockito.when(albumService.findById(1L)).thenThrow(PhotoManagerClientException.class);
		AlbumResource albumResource = new AlbumResource(albumService);
		try {
			albumResource.getAlbumById("1");
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof PhotoManagerClientException), is(true));
		}
	}
	
	@Test
	public void testFindByIdInternalException() {
		Mockito.when(albumService.findById(1L)).thenThrow(NullPointerException.class);
		AlbumResource albumResource = new AlbumResource(albumService);
		try {
			albumResource.getAlbumById("1");
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof InternalErrorException), is(true));
		}
	}
	
	@Test
	public void testFindByIdParameterFormatException() {
		Mockito.when(albumService.findById(1L)).thenThrow(NumberFormatException.class);
		AlbumResource albumResource = new AlbumResource(albumService);
		try {
			albumResource.getAlbumById("hgf543f");
			Assert.fail("Exception should have been thrown");
		} catch (Exception e) {
			assertThat((e instanceof InvalidEntityFetchException), is(true));
		}
	}
	
	private void assertAlbumResourceLinks(ApiAlbum apiAlbum, boolean photoSelfOnly) {
		List<Link> links = apiAlbum.getLinks();
		assertThat(links.isEmpty(), is(false));
		assertThat(links.get(0).getRel(), is("self"));
		assertThat(links.get(0).getHref(), notNullValue());
		assertThat(links.get(1).getRel(), is("addphotos"));
		assertThat(links.get(1).getHref(), notNullValue());
		List<ApiPhoto> photos = apiAlbum.getPhotos();
		for (ApiPhoto apiPhoto : photos) {
			assertPhotoResourceLinks(apiPhoto, photoSelfOnly);
		}
	}
	
	private void assertPhotoResourceLinks(ApiPhoto apiPhoto, boolean selfOnly) {
		List<Link> links = apiPhoto.getLinks();
		assertThat(links.isEmpty(), is(false));
		assertThat(links.get(0).getRel(), is("self"));
		assertThat(links.get(0).getHref(), notNullValue());
		if (!selfOnly){
			assertThat(links.get(1).getRel(), is("album"));
			assertThat(links.get(1).getHref(), notNullValue());
		}
	}

}
