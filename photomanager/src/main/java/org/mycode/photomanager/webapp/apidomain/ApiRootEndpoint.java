/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.apidomain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;

public class ApiRootEndpoint {
	
	private List<Link> links;
	
	public ApiRootEndpoint() {
		this.links = new ArrayList<Link>();
	}
	
	public void addLink(Link link) {
		this.links.add(link);
	}
	
	public List<Link> getLinks() {
		return this.links;
	}

}
