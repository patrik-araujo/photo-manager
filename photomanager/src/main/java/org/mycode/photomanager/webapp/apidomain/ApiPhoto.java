/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.apidomain;

import java.util.ArrayList;
import java.util.List;

import org.mycode.photomanager.core.domain.Photo;
import org.springframework.hateoas.Link;

public class ApiPhoto {

	private Long id;
	private Long albumId;
	private String title;
	private String url;
	private List<Link> links;
	
	public ApiPhoto() {
		this.links = new ArrayList<>();
	}
	
	public ApiPhoto(Long id, Long albumId, String title, String url) {
		this.id = id;
		this.albumId = albumId;
		this.title = title;
		this.url = url;
		this.links = new ArrayList<>();
	}
	
	public ApiPhoto (Photo photo) {
		this(photo.getId(), photo.getAlbumId(), photo.getTitle(), photo.getUrl());
//		if (photo.getAlbum() != null)
//			this.albumId = photo.getAlbum().getId();
	}
	
	public Photo coreEntity() {
		return new Photo(id, url, title, albumId);
	}
	
	public void addLink(Link link) {
		links.add(link);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Long albumId) {
		this.albumId = albumId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
}
