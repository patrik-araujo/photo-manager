/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.config;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyRequestEventListener implements RequestEventListener {
	private final long startTime;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public MyRequestEventListener() {
		startTime = System.currentTimeMillis();
	}

	@Override
	public void onEvent(RequestEvent event) {
		switch (event.getType()) {
		case RESOURCE_METHOD_START:
			logger.info("Resource method " + event.getUriInfo().getMatchedResourceMethod().getHttpMethod() + " for "
					+ "/" + event.getUriInfo().getPath());
			break;
		case FINISHED:
			logger.info(
					"Method finished. Processing time " + event.getUriInfo().getMatchedResourceMethod().getHttpMethod()
							+ " for " + (System.currentTimeMillis() - startTime) + " ms.");
			break;
		default:
			break;
		}
	}
}
