/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import static org.mycode.photomanager.webapp.resource.ResourceLinks.buildPhotoLinks;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.InvalidEntityFetchException;
import org.mycode.photomanager.core.exception.PhotoManagerClientException;
import org.mycode.photomanager.core.service.PhotoService;
import org.mycode.photomanager.webapp.apidomain.ApiPhoto;
import org.mycode.photomanager.webapp.exception.InternalErrorException;

@Named
@Path(PhotoResource.PHOTOS_PATH)
public class PhotoResource {
	public static final String PHOTOS = "photos";
	public static final String PHOTOS_PATH = "/photos";
	public static final String PHOTOS_ID_PATH_TEMPLATE = "{id}";
	public static final String PHOTOS_ID_PATH_PARAM = "id";

	@Context
	UriInfo uriInfo;

	private PhotoService photoService;

	@Inject
	public PhotoResource(PhotoService photoService) {
		this.photoService = photoService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPhotos() {
		try {
			List<Photo> photos = photoService.findAll();
			List<ApiPhoto> apiPhotos = new ArrayList<ApiPhoto>();
			photos.forEach(photo -> {
				ApiPhoto apiPhoto = buildPhotoLinks(new ApiPhoto(photo));
				apiPhotos.add(apiPhoto);
			});
			GenericEntity<List<ApiPhoto>> genericEntity = new GenericEntity<List<ApiPhoto>>(apiPhotos) {
			};
			return Response.ok(genericEntity).build();
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@GET
	@Path(PHOTOS_ID_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPhotoById(@PathParam(PHOTOS_ID_PATH_PARAM) String photoId) {
		try {
			Photo photo = photoService.findById(Long.valueOf(photoId));
			ApiPhoto apiPhoto = new ApiPhoto(photo);
			apiPhoto = ResourceLinks.buildPhotoLinks(apiPhoto);
			return Response.ok(apiPhoto).build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The photo id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createPhoto(ApiPhoto apiPhotoInput) {
		try {
			Photo photo = apiPhotoInput.coreEntity();
			Photo createdPhoto = photoService.createPhoto(photo);
			ApiPhoto apiPhoto = new ApiPhoto(createdPhoto);
			apiPhoto = ResourceLinks.buildPhotoLinks(apiPhoto);
			URI resourceLocation = uriInfo.getAbsolutePathBuilder().path(apiPhoto.getId().toString()).build();
			return Response.created(resourceLocation).entity(apiPhoto).build();
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@PUT
	@Path(PHOTOS_ID_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePhoto(@PathParam(PHOTOS_ID_PATH_PARAM) String photoId, ApiPhoto apiPhotoInput) {
		try {
			Photo photo = apiPhotoInput.coreEntity();
			photo.setId(Long.valueOf(photoId));
			Photo createdPhoto = photoService.updatePhoto(photo);
			ApiPhoto apiPhoto = new ApiPhoto(createdPhoto);
			apiPhoto = ResourceLinks.buildPhotoLinks(apiPhoto);
			return Response.ok(apiPhoto).build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The photo id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@DELETE
	@Path(PHOTOS_ID_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deletePhoto(@PathParam(PHOTOS_ID_PATH_PARAM) String photoId) {
		try {
			Long photoToDeletedId = Long.valueOf(photoId);
			photoService.deletePhoto(photoToDeletedId);
			return Response.ok().build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The photo id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

}
