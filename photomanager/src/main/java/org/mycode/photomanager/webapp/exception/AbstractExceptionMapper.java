/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractExceptionMapper<Type extends Throwable> implements ExceptionMapper<Type>{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Response toResponse(Type exception) {
		logger.error("", exception);
		return generateResponse(exception);
	}
	
	protected Response generateResponse(Type exception) {
		return Response.status(500).entity("Internal Error Processing Exception : " + exception).build();
	}
	
}
