/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import org.mycode.photomanager.webapp.apidomain.ApiAlbum;
import org.mycode.photomanager.webapp.apidomain.ApiPhoto;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.jaxrs.JaxRsLinkBuilder;

public class ResourceLinks {
	public static final String REL_SELF = "self";
	public static final String REL_ADDPHOTOS = "addphotos";
	public static final String REL_ALBUM = "album";
	
	public static ApiAlbum buildAlbumLinks(ApiAlbum apiAlbum) {
		JaxRsLinkBuilder builder = JaxRsLinkBuilder.linkTo(AlbumResource.class).slash(apiAlbum.getId());
		Link selfLink = builder.withSelfRel();
		Link addPhotosLink = builder.slash(PhotoResource.PHOTOS_PATH).withRel(REL_ADDPHOTOS);
		apiAlbum.addLink(selfLink);
		apiAlbum.addLink(addPhotosLink);
		return apiAlbum;
	}
	
	public static ApiPhoto buildBuildPhotoLinksSelfOnly(ApiPhoto apiPhoto) {
		return buildPhotoLinks(apiPhoto, true);
	}
	
	public static ApiPhoto buildPhotoLinks(ApiPhoto apiPhoto) {
		return buildPhotoLinks(apiPhoto, false);
	}
	
	private static ApiPhoto buildPhotoLinks(ApiPhoto apiPhoto, boolean selfOnly) {
		JaxRsLinkBuilder builder = JaxRsLinkBuilder.linkTo(PhotoResource.class).slash(apiPhoto.getId());
		apiPhoto.addLink(builder.withSelfRel());
		if (!selfOnly) {
			Link albumLink = JaxRsLinkBuilder.linkTo(AlbumResource.class).slash(apiPhoto.getAlbumId()).withRel(REL_ALBUM);
			apiPhoto.addLink(albumLink);
		}
		return apiPhoto;
	}

}
