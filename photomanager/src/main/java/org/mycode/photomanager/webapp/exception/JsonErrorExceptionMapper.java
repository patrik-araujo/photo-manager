/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.mycode.photomanager.webapp.apidomain.ApiErrorMessage;

import com.fasterxml.jackson.databind.JsonMappingException;

@Provider
public class JsonErrorExceptionMapper extends AbstractExceptionMapper<JsonMappingException>{

	public Response generateResponse(JsonMappingException e) {
		ApiErrorMessage apiErrorMessage = new ApiErrorMessage(String.valueOf(Status.BAD_REQUEST.getStatusCode()), "INVALID_MESSAGE_FORMAT", e.getMessage());
		return Response.status(Status.BAD_REQUEST).entity(apiErrorMessage).build();
	}

}
