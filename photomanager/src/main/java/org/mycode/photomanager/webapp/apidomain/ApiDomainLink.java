/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.apidomain;

public class ApiDomainLink {
	
	private String href;
	private String rel;
	private String method;
	
	public ApiDomainLink() {
	}

	public ApiDomainLink(String href, String rel, String method) {
		super();
		this.href = href;
		this.rel = rel;
		this.method = method;
	}

	public String getHref() {
		return href;
	}

	public String getRel() {
		return rel;
	}

	public String getMethod() {
		return method;
	}

}
