/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mycode.photomanager.webapp.apidomain.ApiRootEndpoint;
import org.springframework.hateoas.jaxrs.JaxRsLinkBuilder;

@Path("/")
public class RootResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response rootEndpoints() {
		JaxRsLinkBuilder albumsLinkBuilder = JaxRsLinkBuilder.linkTo(AlbumResource.class);
		JaxRsLinkBuilder photosLinkBuilder = JaxRsLinkBuilder.linkTo(PhotoResource.class);
		JaxRsLinkBuilder initLinkBuilder = JaxRsLinkBuilder.linkTo(InitializationResource.class);
		ApiRootEndpoint apiRootEndpoint = new ApiRootEndpoint();
		apiRootEndpoint.addLink(albumsLinkBuilder.withRel(AlbumResource.ALBUMS));
		apiRootEndpoint.addLink(photosLinkBuilder.withRel(PhotoResource.PHOTOS));
		apiRootEndpoint.addLink(initLinkBuilder.withRel(InitializationResource.INIT));
		return Response.ok(apiRootEndpoint).build();
	}

}
