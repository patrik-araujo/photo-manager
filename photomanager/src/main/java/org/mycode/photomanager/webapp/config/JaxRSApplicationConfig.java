/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.config;

import javax.inject.Named;

import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.message.filtering.EntityFilteringFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.EncodingFilter;

@Named
public class JaxRSApplicationConfig extends ResourceConfig {
	public JaxRSApplicationConfig() {
		/* Enables the GZip filter to all endpoints in the project */
		register(EntityFilteringFeature.class);
		EncodingFilter.enableFor(this, GZipEncoder.class);

		/*
		 * Enables the support for the monitoring statistics. It is disable by
		 * default because it has an impact on performance.
		 */
		property(ServerProperties.MONITORING_STATISTICS_ENABLED, true);

		/* Enables the support for the monitoring statistics via MBEANS */
		property(ServerProperties.MONITORING_STATISTICS_MBEANS_ENABLED, true);

		packages("org.mycode.photomanager");
		
		/* 
		 * Registers a simple listener that logs the latency of the method processing the request.
		 * Notice that this is not the request latency, which would take into account the network latency.
		 */
		register(MyApplicationEventListener.class);
		
		setApplicationName("photomanager");
	}
}
