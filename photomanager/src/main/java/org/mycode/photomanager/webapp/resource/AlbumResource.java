/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import static org.mycode.photomanager.webapp.resource.ResourceLinks.buildAlbumLinks;
import static org.mycode.photomanager.webapp.resource.ResourceLinks.buildBuildPhotoLinksSelfOnly;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.InvalidEntityFetchException;
import org.mycode.photomanager.core.exception.PhotoManagerClientException;
import org.mycode.photomanager.core.service.AlbumService;
import org.mycode.photomanager.webapp.apidomain.ApiAlbum;
import org.mycode.photomanager.webapp.apidomain.ApiPhoto;
import org.mycode.photomanager.webapp.exception.InternalErrorException;

@Named
@Path(AlbumResource.ALBUMS_PATH)
public class AlbumResource {
	public static final String ALBUMS = "albums";
	public static final String ALBUMS_PATH = "/albums";
	public static final String ALBUMS_ID_PATH_PARAM = "id";
	public static final String ALBUMS_ID_PATH_TEMPLATE = "{id}";
	public static final String ALBUMS_POST_PHOTO_PATH_TEMPLATE = ALBUMS_ID_PATH_TEMPLATE + "/photos";

	@Context
	UriInfo uriInfo;

	private AlbumService albumService;

	@Inject
	public AlbumResource(AlbumService albumService) {
		this.albumService = albumService;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAlbums() {
		try {
			List<Album> albums = albumService.findAll();
			List<ApiAlbum> apiAlbums = new ArrayList<ApiAlbum>();
			albums.forEach(album -> {
				ApiAlbum apiAlbum = buildAlbumLinks(new ApiAlbum(album));
				apiAlbum.getPhotos().forEach(photo -> buildBuildPhotoLinksSelfOnly(photo));
				apiAlbums.add(apiAlbum);
			});
			GenericEntity<List<ApiAlbum>> genericEntity = new GenericEntity<List<ApiAlbum>>(apiAlbums) {
			};
			return Response.ok(genericEntity).build();
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@GET
	@Path(ALBUMS_ID_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAlbumById(@PathParam(ALBUMS_ID_PATH_PARAM) String albumId) {
		try {
			Album album = albumService.findById(Long.valueOf(albumId));
			ApiAlbum apiAlbum = new ApiAlbum(album);
			apiAlbum = ResourceLinks.buildAlbumLinks(apiAlbum);
			apiAlbum.getPhotos().forEach(photo -> buildBuildPhotoLinksSelfOnly(photo));
			return Response.ok(apiAlbum).build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The album id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAlbum(ApiAlbum apiAlbumInput) {
		try {
			Album album = apiAlbumInput.coreEntity();
			Album createdAlbum = albumService.createAlbum(album);
			ApiAlbum apiAlbum = new ApiAlbum(createdAlbum);
			apiAlbum = ResourceLinks.buildAlbumLinks(apiAlbum);
			apiAlbum.getPhotos().forEach(photo -> buildBuildPhotoLinksSelfOnly(photo));
			URI resourceLocation = uriInfo.getAbsolutePathBuilder().path(apiAlbum.getId().toString()).build();
			return Response.created(resourceLocation).entity(apiAlbum).build();
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@PUT
	@Path(ALBUMS_ID_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAlbum(@PathParam(ALBUMS_ID_PATH_PARAM) String albumId, ApiAlbum apiAlbumInput) {
		try {
			Album album = apiAlbumInput.coreEntity();
			album.setId(Long.valueOf(albumId));
			Album createdAlbum = albumService.updateAlbum(album);
			ApiAlbum apiAlbum = new ApiAlbum(createdAlbum);
			apiAlbum = ResourceLinks.buildAlbumLinks(apiAlbum);
			apiAlbum.getPhotos().forEach(photo -> buildBuildPhotoLinksSelfOnly(photo));
			return Response.ok(apiAlbum).build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The album id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@DELETE
	@Path(ALBUMS_ID_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteAlbum(@PathParam(ALBUMS_ID_PATH_PARAM) String albumId) {
		try {
			Long albumToDeletedId = Long.valueOf(albumId);
			albumService.deleteAlbum(albumToDeletedId);
			return Response.ok().build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The album id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

	@POST
	@Path(ALBUMS_POST_PHOTO_PATH_TEMPLATE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getAlbumByIdAndPhoto(@PathParam(ALBUMS_ID_PATH_PARAM) String albumId, ApiPhoto apiPhotoInput) {
		try {
			Photo photo = apiPhotoInput.coreEntity();
			albumService.addPhoto(Long.valueOf(albumId), photo);
			ApiPhoto apiPhoto = new ApiPhoto(photo);
			apiPhoto = ResourceLinks.buildPhotoLinks(apiPhoto);
			URI resourceLocation = uriInfo.getBaseUriBuilder().path(PhotoResource.class)
					.path(apiPhoto.getId().toString()).build();
			return Response.created(resourceLocation).entity(apiPhoto).build();
		} catch (NumberFormatException e) {
			throw new InvalidEntityFetchException("The album id must be a number");
		} catch (PhotoManagerClientException e) {
			throw e;
		} catch (Exception e) {
			throw new InternalErrorException(e);
		}
	}

}
