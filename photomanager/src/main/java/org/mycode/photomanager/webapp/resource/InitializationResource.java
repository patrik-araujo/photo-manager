/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.resource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.mycode.photomanager.core.service.AlbumService;
import org.mycode.photomanager.core.service.PhotoService;
import org.mycode.photomanager.webapp.apidomain.ApiAlbum;
import org.mycode.photomanager.webapp.apidomain.ApiInitialization;
import org.mycode.photomanager.webapp.apidomain.ApiPhoto;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

@Named
@Path(InitializationResource.INIT_PATH)
public class InitializationResource {
	public static final String INIT = "init";
	public static final String INIT_PATH = "/init";
	
	private AlbumService albumService;
	private PhotoService photoService;
	
	@Inject
	public InitializationResource(AlbumService albumService, PhotoService photoService) {
		this.albumService = albumService;
		this.photoService = photoService;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response initializeData() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
		jacksonProvider.setMapper(objectMapper);
		Client client = ClientBuilder.newClient(new ClientConfig(jacksonProvider));
		
		WebTarget webTarget = client.target("https://jsonplaceholder.typicode.com");
		WebTarget albumsPath = webTarget.path("albums");
		WebTarget photosPath = webTarget.path("photos");
		
		GenericType<List<ApiAlbum>> albumResponseType = new GenericType<List<ApiAlbum>>(){};
		List<ApiAlbum> albums = albumsPath.request(MediaType.APPLICATION_JSON).get(albumResponseType);
		GenericType<List<ApiPhoto>> photoResponseType = new GenericType<List<ApiPhoto>>(){};
		List<ApiPhoto> photos = photosPath.request(MediaType.APPLICATION_JSON).get(photoResponseType);
		
		albums.stream()
			.map(album -> {album.setId(null); return album;})
			.forEach(album -> albumService.createAlbum(album.coreEntity()));
		photos.stream()
			.map(photo -> {photo.setId(null); return photo;})
			.forEach(photo -> photoService.createPhoto(photo.coreEntity()));
		
		ApiInitialization response = new ApiInitialization("Data initialized successfully", albums.size(), photos.size());
		
		return Response.ok(response).build();
	}

}
