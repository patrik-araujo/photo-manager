/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.mycode.photomanager.webapp.apidomain.ApiErrorMessage;

@Provider
public class InternalErrorMapper extends AbstractExceptionMapper<InternalErrorException>{

	public Response generateResponse(InternalErrorException e) {
		ApiErrorMessage apiErrorMessage = new ApiErrorMessage(String.valueOf(Status.INTERNAL_SERVER_ERROR.getStatusCode()), "INTERNAL_ERROR", e.getMessage());
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(apiErrorMessage).build();
	}

}
