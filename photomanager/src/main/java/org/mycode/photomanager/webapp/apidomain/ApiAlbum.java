/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.apidomain;

import java.util.ArrayList;
import java.util.List;

import org.mycode.photomanager.core.domain.Album;
import org.springframework.hateoas.Link;


public class ApiAlbum {
	
	private Long id;
	private String title;
	private List<ApiPhoto> photos;
	private List<Link> links;
	
	public ApiAlbum() {
		this.links = new ArrayList<>();
	}
	
	public ApiAlbum(Long id, String title) {
		this.id = id;
		this.title = title;
		this.photos = new ArrayList<ApiPhoto>();
		this.links = new ArrayList<Link>();
	}
	
	public ApiAlbum(Long id, String title, List<ApiPhoto> photos, List<Link> links) {
		this.id = id;
		this.title = title;
		this.photos = photos;
		this.links = links;
	}
	
	public ApiAlbum (Album album){
		this.id = album.getId();
		this.title = album.getTitle();
		this.photos = new ArrayList<>();
		this.links = new ArrayList<Link>();
		album.getPhotos().forEach(photo -> this.photos.add(new ApiPhoto(photo)));
	}
	
	public Album coreEntity() {
		Album album = new Album(id, title);
		if (photos != null)
			photos.forEach(photo -> album.addPhoto(photo.coreEntity()));
		return album;
	}
	
	public void addLink(Link link) {
		this.links.add(link);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ApiPhoto> getPhotos() {
		return photos;
	}

	public void setPhotos(List<ApiPhoto> photos) {
		this.photos = photos;
	}

	public List<Link> getLinks() {
		return new ArrayList<Link>(links);
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

}
