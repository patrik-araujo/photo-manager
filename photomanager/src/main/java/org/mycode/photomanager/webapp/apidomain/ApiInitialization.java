/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.webapp.apidomain;

public class ApiInitialization {
	
	private String message;
	private Integer albumCount;
	private Integer photoCount;
	
	public ApiInitialization(String message, Integer albumCount, Integer photoCount) {
		this.message = message;
		this.albumCount = albumCount;
		this.photoCount = photoCount;
	}

	public String getMessage() {
		return message;
	}

	public Integer getAlbumCount() {
		return albumCount;
	}

	public Integer getPhotoCount() {
		return photoCount;
	}
	
	

}
