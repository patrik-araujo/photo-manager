package org.mycode.photomanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.system.ApplicationPidFileWriter;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoManagerApplication {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(PhotoManagerApplication.class);
		/*
		 * Adding a listener to output the application pid in a file. Spring
		 * boot doesn't have an elegant way to stop the application, so we'll
		 * have to keep track of the pid and call a KILL command in the linux
		 * system.
		 */
		application.addListeners(new ApplicationPidFileWriter());
		application.run(args);
	}
}
