/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.persistence;

import java.util.List;

import org.mycode.photomanager.core.domain.Photo;

public interface PhotoRepository {
	
	public Photo save(Photo album);
	public void delete(Photo album);
	public Photo update(Photo album);
	public Photo findById(Long id);
	public List<Photo> findAll();
	
}
