/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.EntityNotFoundException;
import org.mycode.photomanager.core.exception.InvalidEntityException;
import org.mycode.photomanager.core.exception.InvalidEntityFetchException;
import org.mycode.photomanager.core.exception.InvalidOperationException;
import org.mycode.photomanager.core.exception.PhotoManagerClientException;
import org.mycode.photomanager.core.persistence.AlbumSpringRepository;
import org.mycode.photomanager.core.persistence.PhotoSpringRepository;

@Named
public class AlbumService {
	public static final String ALBUM_CONTAINS_INVALID_PHOTOS = "Album contains invalid photos";
	public static final String ALBUM_CANNOT_HAVE_ID_CREATION = "Album cannot have an id during creation.";
	public static final String ALBUM_TITLE_AT_LEAST_3_CHARS = "Album title must have at least 3 characters.";
	public static final String TITLE_IS_REQUIRED_FOR_ALBUM = "Title is required for an album.";
	public static final String ALBUM_CONTAINS_PHOTOS = "Album contains photos.";
	public static final String ALBUM_NOT_FOUND_FOR_ID = "Album not found for provided Id.";
	public static final String ALBUM_ID_REQUIRED = "Album Id is required.";
	
	private AlbumSpringRepository albumRepository;
	private PhotoSpringRepository photoRepository;
	private PhotoService photoService;
	
	@Inject
	public AlbumService(AlbumSpringRepository albumRepository, 
			PhotoSpringRepository photoRepository,
			PhotoService photoService) {
		this.albumRepository = albumRepository;
		this.photoRepository = photoRepository;
		this.photoService = photoService;
	}
	
	public Album createAlbum(Album album) {
		if (album.getId() != null)
			throw new InvalidEntityException(ALBUM_CANNOT_HAVE_ID_CREATION);
		validateAlbum(album);
		List<Photo> photos = new ArrayList<Photo>(album.getPhotos());
		album.getPhotos().clear();
		photos.forEach(photo -> album.addPhoto(photo));
		validatePhotosInAlbum(album);
		Album saved = albumRepository.save(album);
		saved = albumRepository.findOne(saved.getId());
		return saved;
	}
	
	public void validatePhotosInAlbum(Album album) {
		for (Photo photo : album.getPhotos()) {
			try {
				photoService.validatePhoto(photo);
			} catch (PhotoManagerClientException e) {
				throw new InvalidEntityException(ALBUM_CONTAINS_INVALID_PHOTOS);
			}
		}
	}
	
	public Album updateAlbum(Album album) {
		if (album.getId() == null)
			throw new InvalidEntityException(ALBUM_ID_REQUIRED);
		validateAlbum(album);
		List<Photo> photos = new ArrayList<Photo>(album.getPhotos());
		album.getPhotos().clear();
		photos.forEach(photo -> album.addPhoto(photo));
		validatePhotosInAlbum(album);
		Album existingAlbum = albumRepository.findOne(album.getId());
		if (existingAlbum == null)
			throw new InvalidOperationException(ALBUM_NOT_FOUND_FOR_ID);
		Album updated = albumRepository.save(album);
		updated = albumRepository.findOne(updated.getId());
		return updated;
	}
	
	public void deleteAlbum(Long albumId) {
		if (albumId == null)
			throw new InvalidOperationException(ALBUM_ID_REQUIRED);
		Album albumToDelete = albumRepository.findOne(albumId);
		if (albumToDelete == null)
			throw new InvalidOperationException(ALBUM_NOT_FOUND_FOR_ID);
		if (!albumToDelete.getPhotos().isEmpty())
			throw new InvalidEntityException(ALBUM_CONTAINS_PHOTOS);
		albumRepository.delete(albumToDelete);
	}
	
	public void addPhoto(Long albumId, Photo photo) {
		Album found = albumRepository.findOne(albumId);
		if (found == null)
			throw new InvalidOperationException(ALBUM_NOT_FOUND_FOR_ID);
		found.addPhoto(photo);
		photoService.validatePhoto(photo);
		photoRepository.save(photo);
	}
	
	public Album findById(Long albumId) {
		if (albumId == null)
			throw new InvalidEntityFetchException(ALBUM_ID_REQUIRED);
		Album found = albumRepository.findOne(albumId);
		if (found == null)
			throw new EntityNotFoundException(ALBUM_NOT_FOUND_FOR_ID);
		return found;
	}
	
	public List<Album> findAll() {
		Iterable<Album> iterable = albumRepository.findAll();
		List<Album> albums = new ArrayList<Album>();
		iterable.forEach(album -> albums.add(album));
		return albums;
	}
	
	public void validateAlbum(Album album) {
		if (album.getTitle() == null || album.getTitle().isEmpty()) {
			throw new InvalidEntityException(TITLE_IS_REQUIRED_FOR_ALBUM);
		}
		if (album.getTitle().length() < 3) {
			throw new InvalidEntityException(ALBUM_TITLE_AT_LEAST_3_CHARS);
		}
	}
	
}
