/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Photo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private String url;
	private String title;
	
	@Column(name = "my_album_id", insertable = false, updatable = false)
	private Long albumId;
	
	@ManyToOne
	@JoinColumn(name = "my_album_id")
	private Album album;
	
	protected Photo() {
	}

	public Photo(Long id, String title, String url) {
		this.id = id;
		this.url = url;
		this.title = title;
	}
	
	public Photo(String url, String title, Long albumId) {
		this.url = url;
		this.title = title;
		this.albumId = albumId;
	}

	public Photo(Long id, String url, String title, Long albumId) {
		this.id = id;
		this.url = url;
		this.title = title;
		this.albumId = albumId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Long albumId) {
		this.albumId = albumId;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
