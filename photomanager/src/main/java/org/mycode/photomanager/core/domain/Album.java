/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Album {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String title;
	
	@OneToMany(mappedBy = "album", orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Photo> photos;
	
	protected Album() {
	}

	public Album(Long id, String title) {
		this.id = id;
		this.title = title;
		this.photos = new ArrayList<>();
	}
	
	public Album(String title) {
		this.title = title;
		this.photos = new ArrayList<>();
	}
	
	public void addPhoto(Photo photo) {
		photo.setAlbumId(id);
		photo.setAlbum(this);
		this.photos.add(photo);
	}
	
	public List<Photo> getPhotos() {
		return photos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
