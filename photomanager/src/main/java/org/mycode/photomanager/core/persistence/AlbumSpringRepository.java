/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.persistence;

import org.mycode.photomanager.core.domain.Album;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AlbumSpringRepository extends PagingAndSortingRepository<Album, Long>{

}
