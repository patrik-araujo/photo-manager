/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mycode.photomanager.core.domain.Album;
import org.mycode.photomanager.core.domain.Photo;
import org.mycode.photomanager.core.exception.EntityNotFoundException;
import org.mycode.photomanager.core.exception.InvalidEntityException;
import org.mycode.photomanager.core.exception.InvalidEntityFetchException;
import org.mycode.photomanager.core.exception.InvalidOperationException;
import org.mycode.photomanager.core.persistence.AlbumSpringRepository;
import org.mycode.photomanager.core.persistence.PhotoSpringRepository;

@Named
public class PhotoService {
	public static final String PHOTO_CANNOT_HAVE_ID_CREATION = "Photo cannot have an id during creation.";
	public static final String URL_REQUIRED_FOR_PHOTO = "Url is required for a photo.";
	public static final String PHOTO_TITLE_AT_LEAST_3_CHARS = "Photo title must have at least 3 characters.";
	public static final String TITLE_REQUIRED_FOR_PHOTO = "Title is required for a photo.";
	public static final String ALBUM_REQUIRED_FOR_PHOTO = "Album is required for a photo.";
	public static final String PHOTO_ID_REQUIRED = "Photo Id is required.";
	public static final String ALBUM_DOES_NOT_EXIST = "Album does not exist.";
	public static final String ALBUM_NOT_FOUND_FOR_ID = "Album not found for provided Id.";
	public static final String PHOTO_NOT_FOUND_FOR_ID = "Photo not found for provided Id.";

	private PhotoSpringRepository photoRepository;
	private AlbumSpringRepository albumRepository;
	
	@Inject
	public PhotoService(PhotoSpringRepository photoRepository,
			AlbumSpringRepository albumRepository) {
		this.photoRepository = photoRepository;
		this.albumRepository = albumRepository;
	}
	
	public Photo createPhoto(Photo photo) {
		if (photo.getId() != null)
			throw new InvalidEntityException(PHOTO_CANNOT_HAVE_ID_CREATION);
		validatePhoto(photo);
		if (photo.getAlbumId() == null)
			throw new InvalidEntityException(ALBUM_REQUIRED_FOR_PHOTO);
		Album album = albumRepository.findOne(photo.getAlbumId());
		if (album == null) {
			throw new InvalidEntityException(ALBUM_NOT_FOUND_FOR_ID);
		}
		photo.setAlbum(album);
		Photo saved = photoRepository.save(photo);
		saved = photoRepository.findOne(saved.getId());
		return saved;
	}
	
	public Photo updatePhoto(Photo photo) {
		if (photo.getId() == null)
			throw new InvalidEntityException(PHOTO_ID_REQUIRED);
		validatePhoto(photo);
		if (photo.getAlbumId() == null)
			throw new InvalidEntityException(ALBUM_REQUIRED_FOR_PHOTO);
		Album album = albumRepository.findOne(photo.getAlbumId());
		if (album == null)
			throw new InvalidEntityException(ALBUM_NOT_FOUND_FOR_ID);
		Photo existingPhoto = photoRepository.findOne(photo.getId());
		if (existingPhoto == null)
			throw new InvalidOperationException(PHOTO_NOT_FOUND_FOR_ID);
		photo.setAlbum(album);
		Photo saved = photoRepository.save(photo);
		saved = photoRepository.findOne(saved.getId());
		return saved;
	}
	
	public void deletePhoto(Long photoId) {
		if (photoId == null)
			throw new InvalidOperationException(PHOTO_ID_REQUIRED);
		Photo photoToDelete = photoRepository.findOne(photoId);
		if (photoToDelete == null)
			throw new InvalidOperationException(PHOTO_NOT_FOUND_FOR_ID);
		photoToDelete.setAlbum(null);
		photoRepository.delete(photoToDelete);
	}
	
	public Photo findById(Long photoId) {
		if (photoId == null)
			throw new InvalidEntityFetchException(PHOTO_ID_REQUIRED);
		Photo found = photoRepository.findOne(photoId);
		if (found == null)
			throw new EntityNotFoundException(PHOTO_NOT_FOUND_FOR_ID);
		return found;
	}
	
	public List<Photo> findAll() {
		Iterable<Photo> iterable = photoRepository.findAll();
		List<Photo> photos = new ArrayList<Photo>();
		iterable.forEach(photo -> photos.add(photo));
		return photos;
	}
	
	public void validatePhoto(Photo photo) {
		if (photo.getTitle() == null || photo.getTitle().isEmpty()) {
			throw new InvalidEntityException(TITLE_REQUIRED_FOR_PHOTO);
		}
		if (photo.getTitle().length() < 3) {
			throw new InvalidEntityException(PHOTO_TITLE_AT_LEAST_3_CHARS);
		}
		if (photo.getUrl() == null || photo.getUrl().isEmpty()) {
			throw new InvalidEntityException(URL_REQUIRED_FOR_PHOTO);
		}
	}
}
