/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.exception;

public class InvalidEntityException extends PhotoManagerClientException{
	private static final long serialVersionUID = 6808542653889158318L;

	public InvalidEntityException(String message) {
        super(message);
    }
	
	public InvalidEntityException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public InvalidEntityException(Throwable cause) {
        super(cause);
    }
	
}
