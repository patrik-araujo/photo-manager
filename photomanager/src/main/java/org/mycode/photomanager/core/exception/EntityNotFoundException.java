/*******************************************************************************
 * Copyright (C) 2017 Patrik Araujo - All Rights Reserved
 * You may only use this code for the purposes authorized
 * by the Author. This code cannot be distributed without
 * consent of the Author. The author is not responsible 
 * by how this software is used.
 *******************************************************************************/
package org.mycode.photomanager.core.exception;

public class EntityNotFoundException extends PhotoManagerClientException{
	private static final long serialVersionUID = -4809056904870642335L;

	public EntityNotFoundException(String message) {
        super(message);
    }
	
	public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public EntityNotFoundException(Throwable cause) {
        super(cause);
    }
	
}
